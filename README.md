# Readme #

This repo contains all code and data used to add an annotation layer with normalization action categories to the LexNorm2015 dataset (https://github.com/noisy-text/noisy-text.github.io/tree/master/2015/files)

Below we describe each file of this repo:

### data ###

* agreement.all: agreement of all 150 words
* agreement.diff: only the disagreements
* agreement.diff.txt: the disagreements with the replacement pairs
* lexnorm2015.annotated: the final annotation merged into the lexnorm2015 file with scripts/copyBack.py (THIS IS PROBABLY THE FILE YOU ARE LOOKING FOR) note that the order of the categories is slightly different compared to the paper.
* lexnorm2015.train: original lexnorm data, converted from json with scripts/json2norm.py
* pairsAnnotated_fixed.txt: the final annotation of the replacement pairs (after settling annotator disagreements)
* pairsAnnotated: annotation by first annotator: please note that the order of the categories is changed afterwards
* rawTweets: the text of the Tweets
* secondAnnotator: annotation by second annotator: please note that the order of the categories is changed afterwards
* spss: spps output (kappa)
* spss.spv: spss file
* taxonomy: the used taxonomy, and some notes from annotation

### output ###

* the output from scripts/distribution.py and scripts/testPerf.py for the paper

### scripts ###
* 1.distribution.graph.py: to generate the distribution graph 
* 2.norm.prep.sh: installs MoNoise
* 2.runMonoise.py: get prediction from MoNoise
* 2.graphIntended.py: generate graph used in my thesis
* 2.graphPerf.py: generate graph used in my thesis
* agreement.py: takes both annotation files and produces agreement.* files
* copyBack.py: insert annotation from pairs back into the Tweets
* filter.py: filter out categories from the training set; very useful to exclude phrasal abbreviations (cat 6)"
* findErrs.py: collect normalization pairs from normalization corpus, doubles can be removed with: "| sort | uniq"
* json2norm.py: convert json format to a conll like format
* myutils.py: used for the graphs
* rob.mplstyle: used for the graphs


### reproduce ###

To reproduce all graphs, run:
```
#!bash
./scripts/2.norm.prep.sh
python3 scripts/2.runMonoise.py > run.sh
./run.sh
python3 scripts/1.distribution.graph.py
python3 scripts/2.graphIntended.py
python3 scripts/2.graphPerf.py
```


## Reproducability
I have adapted this repo for my thesis, if you would like to get the results from the paper, please revert to commit eee0169

