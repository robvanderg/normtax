import sys

if len(sys.argv) < 6:
    print("please specify input file, job name and for each command: time(hours), memory(gb), #cpus")
    exit(1)

def createJob(idx, cmd):
    name = sys.argv[2] + '.' + str(idx)
    print("creating: " + name)
    outFile = open(name, 'w')
    outFile.write("#!/bin/bash\n")
    outFile.write('\n')
    outFile.write("#SBATCH --time=" + sys.argv[3] + ":00:00\n")
    outFile.write("#SBATCH --nodes=1\n")
    outFile.write("#SBATCH --ntasks=1\n")
    outFile.write("#SBATCH --mem=" + sys.argv[4] + 'G\n')
    outFile.write("#SBATCH --cpus-per-task=" + sys.argv[5] + '\n')
    #outFile.write("#SBATCH --mail-type=BEGIN,END,FAIL\n")
    #outFile.write("#SBATCH --mail-user=robvanderg@live.nl\n")
    outFile.write("#SBATCH --job-name=" + name + '\n')
    outFile.write("#SBATCH --output=" + name + '.log\n')
    #outFile.write("#SBATCH --partition=himem\n")
    outFile.write("\n")
    outFile.write(cmd + "\n")
    outFile.close()


idx = 1
for line in open(sys.argv[1]):
    line = line.strip()
    if len(line) > 2 and line[0] != '#':
        createJob(idx, line)
        idx += 1
