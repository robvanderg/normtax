import sys

if len(sys.argv) < 3:
    print("please give taxonomy annotation and normalization file, eg. pairsAnnotated_fixed.txt lexnorm2015.train")
    exit()

conversions = {}
for line in open (sys.argv[1]):
    splitted = line.split()
    orig = splitted[1]
    cor = splitted[-2]
    if len(splitted) > 5:
        cor = ' '.join(splitted[3:-1])
    if  '->' in cor:
        cor = ''
    conversions[orig + ' ' + cor] = splitted[-1]

#for conv in conversions:
#    print (conv, '-', conversions[conv])

for line in open(sys.argv[2]):
    splitted = line.split()
    if len(splitted) < 2:
        print(line, end='')
        continue
    orig = splitted[0].lower()
    cor = splitted[-1].lower()
    if len(splitted) > 3:
        cor = ' '.join(splitted[2:-1]) + ' ' + splitted[-1]
    if len(splitted) == 2:
        cor = ''
    if orig == cor:
        print('0\t' + line, end='')
        continue
    
    cat = conversions[orig + ' ' + cor] 
    print(str(cat) + '\t' + line, end='')
    


