import sys
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import myutils

if len(sys.argv) < 3:
    print('please give paths for:\n - normalization for system \n - data/lexnorm2015.annotated')
    print('python3 scripts/2.graphPerf.py preds/output.11111111111 data/lexnorm2015.annotated')
    exit(1)

fig, ax = plt.subplots(figsize=(8,5), dpi=300)

correct = [0] * 15
wrongRank = [0] * 15
totals = [0] * 15

incorNormed = 0


predFile = open(sys.argv[1])
gold = []
for lineGold in open(sys.argv[2]):
    goldTok = lineGold.split()
    if len(goldTok) > 2:
        if len(goldTok) == 3:
            goldTok = goldTok[:2] + [''] + [goldTok[-1]]
        if len(goldTok) > 4:
            goldTok = goldTok[:3] + [' '.join(goldTok[3:])]
        goldTok[1] = goldTok[1].lower()
        goldTok[3] = goldTok[3].lower()
        gold.append(goldTok)
    elif len(goldTok) < 2 and len(gold) > 0:
        pred = []
        for linePred in predFile:
            if linePred == '.\n':
                break
            elif len(linePred) > 2:
                predTok = linePred.split(' ')
                pos = int(predTok[0])
                if pos >= len(pred):
                    pred.append([])
                pred[pos].append(' '.join(predTok[1:-2]).lower())
        if len(gold) != len(pred):
            print('Error: ' + str(len(gold)) + '\t!=\t' + str(len(pred)))
        else:
            for i in range(len(gold)):
                corNorm = gold[i][3]
                cat = int(gold[i][0])
                totals[cat] += 1
                if cat == 0:
                    if pred[i][0] != corNorm:
                        incorNormed += 1
                elif pred[i][0] == corNorm:
                    correct[cat] += 1
                    wrongRank[cat] += 1
                elif pred[i][0] == gold[i][1] and pred[i][1] == corNorm:
                    wrongRank[cat] += 1
                else:
                    for predCand in pred[i]:
                        if predCand == corNorm:
                            wrongRank[cat] += 1
                            break
        gold = []

myutils.setTicks(ax, myutils.cats, 45)

ax.set_xticks(range(len(myutils.cats)))
print(totals)
print(wrongRank)
print(correct)
barwidth = 2/3
idxs = []
for i in range(len(totals)-1):
    idxs.append(i + .5)
print(idxs)
ax.bar(idxs, totals[1:], width = barwidth, color='firebrick', label='Not Found')
ax.bar(idxs, wrongRank[1:], width = barwidth, color='darkorange', label='Wrong Rank')
#ax.bar(idxs, goldErrDet[1:], width = .7, color='gold', label='ErrDet')
ax.bar(idxs, correct[1:], width = barwidth, color='yellowgreen', label='Correct')

ax.set_ylabel("Number of replacements")
#ax.set_xlabel("Category", color='black')
ax.set_xlim(0, 14)
leg = plt.legend(loc='upper left', fontsize=13)
leg.get_frame().set_linewidth(1.5)
fig.savefig('type.pdf', bbox_inches='tight')
#plt.show()

