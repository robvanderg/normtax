# If more than 5 arguments are given, it will run using slurm
function run {
    if [ "$6" ]; 
    then
        python3 scripts/pg.prep.py $1 $2 $3 $4 $5
        python3 scripts/pg.run.py $2.[0-9]* 
    else
        chmod +x $1
        bash $1
    fi
}

# Download MoNoise
./scripts/0.norm.prep.sh

# Run MoNoise
python3 ./scripts/0.norm.run.py > runMoNoise.sh
run ./runMoNoise.sh monoise 5 50 8 $1


