import json
import sys

if len(sys.argv) < 2:
    print("please specify json file")
    exit(0)
with open(sys.argv[1]) as data_file:    
    data = json.load(data_file)

norms = {}
 
counter = 0
for sent in data:
    print(counter)
    for wordIdx in range(len(sent['input'])):
        print(sent['input'][wordIdx], end='\t')
        if sent['input'][wordIdx].lower() == sent['output'][wordIdx].lower():
            print('IV', end='\t')
        else:
            print('OOV', end='\t')
        print(sent['output'][wordIdx])
    counter += 1 
