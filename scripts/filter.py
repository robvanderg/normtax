import sys

if len(sys.argv) < 3:
    print("please give file and list of categories to exclude, for example:")
    print("python3 ../scripts/filter.py lexnorm2015.annotated 6")
    exit(1)

exclude = set()
for i in range(2,len(sys.argv)):
    exclude.add(int(sys.argv[i]))

for line in open(sys.argv[1]):
    tok = line.strip().split('\t')
    if len(tok) < 4:
        print(line.strip())
    elif (int(tok[0]) in exclude):
        print(tok[1] + '\t' + tok[2] + '\t' + tok[1])
    else:
        print(tok[1] + '\t' + tok[2] + '\t' + tok[3])

