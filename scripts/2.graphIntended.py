import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import sys
import myutils

if len(sys.argv) < 2:
    print('please give paths for annotated data\n')
    print('python3 scripts/2.graphIntended.py data/lexnorm2015.annotated')
    exit(1)

plt.style.use('scripts/rob.mplstyle')
fig, ax = plt.subplots(figsize=(8,5), dpi=300)


def getFound(filePath):
    found = [0] * 15
    totals = [0] * 15
    
    incorNormed = 0
    sents = 0
    
    predFile = open(filePath)
    gold = []
    for lineGold in open(sys.argv[1]):
        goldTok = lineGold.split()
        if len(goldTok) > 2:
            if len(goldTok) == 3:
                goldTok = goldTok[:2] + [''] + [goldTok[-1]]
            if len(goldTok) > 4:
                goldTok = goldTok[:3] + [' '.join(goldTok[3:])]
            goldTok[1] = goldTok[1].lower()
            goldTok[3] = goldTok[3].lower()
            gold.append(goldTok)
        elif len(goldTok) < 2 and len(gold) > 0:
            pred = []
            for linePred in predFile:
                if linePred == '.\n':
                    break
                elif len(linePred) > 2:
                    predTok = linePred.split(' ')
                    pos = int(predTok[0])
                    if pos >= len(pred):
                        pred.append([])
                    pred[pos].append(' '.join(predTok[1:-2]).lower())
            if len(gold) != len(pred):
                print(str(len(gold)) + '\t!=\t' + str(len(pred)))
                pass
            else:
                sents += 1
                for i in range(len(gold)):
                    corNorm = gold[i][3]
                    cat = int(gold[i][0])
                    totals[cat]+= 1
                    if cat == 0:
                        if pred[i][0] != corNorm:
                            incorNormed += 1
                    for candIdx, cand in enumerate(pred[i]):
                        if cand == corNorm:
                            found[cat] += 1
                            break
                    #if len(pred[i]) > 1 and pred[i][1] == corNorm:
                    #    found[cat] += 1
            gold = []
    newFound = [0,0]
    for i in range(2,6):
        newFound[0] += found[i]
    for i in range(6,len(found)):
        newFound[1] += found[i]
    return newFound

correctW2V = getFound('preds/output.10000000000')
correctAsp = getFound('preds/output.01000000000')
correctLookup = getFound('preds/output.00100000000')
correctBase = getFound('preds/output.11111111111') 

barwidth = 1/4
idx1 = []
idx2 = []
idx3 = []
for i in range(len(correctW2V)):
    idx1.append(barwidth + i + 0 * barwidth)
    idx2.append(barwidth + i + 1 * barwidth)
    idx3.append(barwidth + i + 2 * barwidth)

print(correctW2V, idx1)
print(correctAsp, idx2)
print(correctLookup, idx3)
print(correctBase)

myutils.setTicks(ax, ['Unintended', 'Intended'], 0)
ax.bar(idx1, correctW2V, width = barwidth, color=myutils.colors[0], label=myutils.featNames[0])
ax.bar(idx2, correctAsp, width = barwidth, color=myutils.colors[1], label=myutils.featNames[1])
ax.bar(idx3, correctLookup, width = barwidth, color=myutils.colors[2], label=myutils.featNames[2])
for i in range(0, len(correctBase)):
    if i == 1:
        ax.plot([i , i+1], [correctBase[i]] * 2, color='black', linewidth=3, label='All')
    else:
        ax.plot([i , i+1], [correctBase[i]] * 2, color='black', linewidth=3)

leg = plt.legend(loc='upper left')
leg.get_frame().set_linewidth(1.5)
#ax.set_xlabel("Category", color='black')
ax.set_ylabel("Number of tokens")
ax.set_xlim(0,2)
fig.savefig('intended.pdf', bbox_inches='tight')
#plt.show()

