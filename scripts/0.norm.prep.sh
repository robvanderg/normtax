################# Prepare MoNoise ################
cd ..

if [ ! -d "monoise" ]; then
    git clone https://robvanderg@bitbucket.org/robvanderg/monoise.git 

    cd monoise
    mkdir -p working
    mkdir -p data
    cd data
    curl www.robvandergoot.com/data/monoise/en.tar.gz | tar xvz

    cd ../src
    wget http://www.hlt.utdallas.edu/~chenli/normalization/2577_tweets
    cd ../../
fi

cd monoise/src
icmbuild

