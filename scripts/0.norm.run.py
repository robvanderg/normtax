
allStr = '00000000000'
for i in [0,1,2,5,99]:#range(len(allStr)):
    featStr = allStr[:i] + '1' + allStr[i+1:]
    if i == 99:
        featStr = allStr.replace('0','1')
    cmd = './tmp/bin/binary -m KF -r ../working/kfold.' + featStr + ' -i ../../normtax/data/lexnorm2015.train -d ../data/en/ -K 10 -o ../../normtax/preds/output.' + featStr + ' -c 9999 -n 8 -f ' + featStr
    print('cd ../monoise/src && ' + cmd)
