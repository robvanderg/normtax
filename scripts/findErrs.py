import sys

for line in open(sys.argv[1], encoding='utf-8', errors='ignore'):
    splitted = line.split('\t')
    if len(splitted) < 2:
        pass
    else:
        if splitted[0].strip().lower() != splitted[2].strip().lower():
            print('\t', splitted[0].strip().lower(), '\t->\t', splitted[2].strip().lower())

